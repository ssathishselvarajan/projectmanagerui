import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { AppComponent } from './app.component';
import { ViewtaskHandler } from './ProjectManager/ViewTaskhandler/viewtaskHandler.component';
import { AddtaskhandlerComponent } from './ProjectManager/addtaskhandler/addtaskhandler.component';
import{MenuComponent } from './ProjectManager/menu/menu.component'
import{ AddprojectmanagerComponent } from  './ProjectManager/addprojectmanager/addprojectmanager.component'
import{ AdduserComponent} from './ProjectManager/adduser/adduser.component'


const appRoutes: Routes = [
    {path : 'AddProject', component : AddprojectmanagerComponent},
    {path : 'AddTask/:TaskID', component : AddtaskhandlerComponent},
    {path : 'AddTask', component : AddtaskhandlerComponent},
    {path : 'AddUser', component : AdduserComponent},
    {path : 'ViewTask', component : ViewtaskHandler},
    {path : '', component : AddprojectmanagerComponent},
    {path : '', component : MenuComponent},
    ];

    @NgModule({
      imports: [RouterModule.forRoot(appRoutes)],
      exports: [RouterModule]
    })
    export class AppRoutingModule { }
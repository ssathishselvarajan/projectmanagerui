import { Injectable } from '@angular/core';
import{ ProjectTable,ProjectDetails} from 'src/app/Model/Projecttable'
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';
import { Console } from '@angular/core/src/console';
import { HttpParams } from '@angular/common/http/src/params';
import { TaskTable } from 'src/app/model/taskTable';
import{ UserTable} from 'src/app/model/Usertable'
import { ParentTaskTable } from '../Model/ParentTask';

const httpOptions = {headers : new HttpHeaders({'Content-Type' : 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class ProjectmanagerAPIService {

  constructor(private http:HttpClient) { }

  getProjects():Observable<ProjectTable[]>{
    return this.http.get<ProjectTable[]>('http://localhost/ProjectManagerAPIServices/api/project/' + 'GetAllProjects');
    //  .pipe(map((res:Response)=> res.json()));
  }

  getProjectByID(projectID: string):Observable<ProjectTable>{
    return this.http.get<ProjectTable>('http://localhost/ProjectManagerAPIServices/api/project/' + 'GetProjectByID',{params : {id:projectID}});
  }

  getProjectDetails():Observable<ProjectDetails[]>{
    return this.http.get<ProjectDetails[]>('http://localhost/ProjectManagerAPIServices/api/project/' + 'GetAllProjectDetails');
  }


  addProject(projectToAdd : ProjectDetails) : Observable<ProjectDetails>{
    debugger;
     console.log(JSON.stringify({ projectToAdd }));
     return this.http.post<ProjectDetails>('http://localhost/ProjectManagerAPIServices/api/project/' + 'AddProject',  projectToAdd, httpOptions );
  }

  updateProject(projectToUpdate : ProjectDetails):Observable<ProjectDetails>{
    return this.http.put<ProjectDetails>('http://localhost/ProjectManagerAPIServices/api/project/' + 'UpdateProject',  projectToUpdate, httpOptions);
  }

  deleteProject(projectID: string):Observable<any>{
      return this.http.delete<any>('http://localhost/ProjectManagerAPIServices/api/project/' + 'DeleteProject',{params : {id:projectID}});
  }


  ///Task Service

  getTasks():Observable<TaskTable[]>{
    return this.http.get<TaskTable[]>('http://localhost/ProjectManagerAPIServices/api/project/' + 'GetAllTasks');
    //  .pipe(map((res:Response)=> res.json()));
  }

  getParentTasks():Observable<ParentTaskTable[]>{
    return this.http.get<ParentTaskTable[]>('http://localhost/ProjectManagerAPIServices/api/project/' + 'GetParentAllTasks');
    //  .pipe(map((res:Response)=> res.json()));
  }

  getTaskByID(taskID: string):Observable<TaskTable>{
    return this.http.get<TaskTable>('http://localhost/ProjectManagerAPIServices/api/project/' + 'GetTaskByID',{params : {id:taskID}});
  }

  getTasksByProjectID(projectID: string):Observable<TaskTable[]>{
    return this.http.get<TaskTable[]>('http://localhost/ProjectManagerAPIServices/api/project/' + 'GetAllTasksByProjectID',{params : {id:projectID}});
  }

  addTask(taskToAdd : TaskTable) : Observable<TaskTable>{
     console.log(JSON.stringify({ taskToAdd }));
     return this.http.post<TaskTable>('http://localhost/ProjectManagerAPIServices/api/project/' + 'AddTask',  taskToAdd, httpOptions );
  }

  updateTask(taskToUpdate : TaskTable):Observable<TaskTable>{
    return this.http.put<TaskTable>('http://localhost/ProjectManagerAPIServices/api/project/' + 'UpdateTask',  taskToUpdate, httpOptions);
  }

  deleteTask(taskID: string):Observable<any>{
      return this.http.delete<any>('http://localhost/ProjectManagerAPIServices/api/project/' + 'DeleteTask',{params : {id:taskID}});
  }

  //User Service

  getUsers():Observable<UserTable[]>{
    debugger;
    return this.http.get<UserTable[]>('http://localhost/ProjectManagerAPIServices/api/project/' + 'GetAllUsers');
    //  .pipe(map((res:Response)=> res.json()));
  }

  getUserByID(userID: string):Observable<UserTable>{
    return this.http.get<UserTable>('http://localhost/ProjectManagerAPIServices/api/project/' + 'GetUserByID',{params : {id:userID}});
  }


  addUser(userToAdd : UserTable) : Observable<UserTable>{
    debugger;
     console.log(JSON.stringify({ userToAdd }));
     return this.http.post<UserTable>('http://localhost/ProjectManagerAPIServices/api/project/' + 'AddUser',  userToAdd, httpOptions );
  }

  updateUser(userToUpdate : UserTable):Observable<UserTable>{
    return this.http.put<UserTable>('http://localhost/ProjectManagerAPIServices/api/project/' + 'UpdateUser',  userToUpdate, httpOptions);
  }

  deleteUser(userID: string):Observable<any>{
      return this.http.delete<any>('http://localhost/ProjectManagerAPIServices/api/project/' + 'DeleteUser',{params : {id:userID}});
  }
}

import { TestBed,inject } from '@angular/core/testing';

import { ProjectmanagerAPIService } from './projectmanager-api.service';
import{ HttpClientModule } from '@angular/common/http';


describe('ProjectmanagerAPIService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[HttpClientModule],
      providers: [ProjectmanagerAPIService]
    });
  });

  it('should be created', inject([ProjectmanagerAPIService], (service: ProjectmanagerAPIService) => {
    expect(service).toBeTruthy();
  }));
});

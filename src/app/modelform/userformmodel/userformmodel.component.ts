import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Input } from '@angular/core';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { FormGroup,FormControl, Validators } from '@angular/forms';
import { Output } from '@angular/core';
import { TaskTable } from 'src/app/Model/Tasktable';
import { ProjectTable } from 'src/app/model/projectTable';
import { UserTable } from 'src/app/model/userTable';
import { ParentTaskTable } from 'src/app/model/ParentTask';
import { ProjectmanagerAPIService } from 'src/app/Service/projectmanager-api.service';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-userformmodel',
  templateUrl: './userformmodel.component.html',
  styleUrls: ['./userformmodel.component.css']
})
export class UserformmodelComponent implements OnInit {

  @Input() searchType: string;
  @Output() searchResult = new EventEmitter<any>();
  allTask : TaskTable[];
  allProject : ProjectTable[];
  allUser : UserTable[];
  allParentTask:ParentTaskTable[];

  selectedTask : TaskTable;
  selectedProject : ProjectTable;
  selectedUser : UserTable;
  selectedparentTask : ParentTaskTable;

  
  searchForm = new FormGroup({
  })

  txtSearchTarget : string;
  constructor(public activeModal: NgbActiveModal, private projectService: ProjectmanagerAPIService) { }

  ngOnInit() {
    this.loadData();
  }

  loadData(){
    if(this.searchType == "user"){
      this.getAllUsers();
    }
    else if(this.searchType =="project"){
      this.getAllProjects();
    }
    else if(this.searchType =="task"){
      this.getAllTasks();
    }
    else if(this.searchType =="taskparent"){
      this.getAllParentTasks();
    }
  }

  getAllUsers(){
    this.projectService.getUsers().subscribe(data =>{this.allUser = data});
 }

  getAllTasks(){
  this.projectService.getTasks().subscribe(data =>{this.allTask = data});
}

getAllParentTasks(){
  this.projectService.getParentTasks().subscribe(data =>{this.allParentTask = data});
}

  getAllProjects(){
  this.projectService.getProjects().subscribe(data =>{this.allProject = data});
  debugger;
}

  closeModal() {
    this.activeModal.close('Modal Closed');
  }

  public submitForm() {
    if(this.searchType == "user"){
      this.searchResult.emit(this.selectedUser);
      this.activeModal.close(this.selectedUser);
     }
     else if(this.searchType =="project"){
       this.searchResult.emit(this.selectedProject);
       this.activeModal.close(this.selectedProject);
     }
     else if(this.searchType =="task"){
      this.searchResult.emit(this.selectedTask);
      this.activeModal.close(this.selectedTask);
     }

     else if(this.searchType =="taskparent"){
      this.searchResult.emit(this.selectedparentTask);
      this.activeModal.close(this.selectedparentTask);
     }
     else{
      this.activeModal.close();
     }
  }

  public SelectData(event: any, selectedData : any){
    if(event.target.checked){
      if(this.searchType == "user"){
       this.selectedUser = selectedData;
      }
      else if(this.searchType =="project"){
        this.selectedProject = selectedData
      }
      else if(this.searchType =="task"){
       this.selectedTask = selectedData;
      }
      else if(this.searchType =="taskparent"){
        this.selectedparentTask = selectedData;
       }
    }
  }

}

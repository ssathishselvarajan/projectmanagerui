import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserformmodelComponent } from './userformmodel.component';
import { FilterpipePipe } from 'src/app/Pipe/filterpipe.pipe';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import{NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {HttpClientModule} from '@angular/common/http';


describe('UserformmodelComponent', () => {
  let component: UserformmodelComponent;
  let fixture: ComponentFixture<UserformmodelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[FormsModule, ReactiveFormsModule,RouterTestingModule, NgbModule, HttpClientModule],
      declarations: [ UserformmodelComponent, FilterpipePipe   ],
      providers:[NgbActiveModal]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserformmodelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

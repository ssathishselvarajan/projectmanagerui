import { UserTable } from "src/app/model/userTable";

export class ProjectTable{
    public  PROJECT_ID : string
    public  PROJECT1 : string
    public  START_DATE : string
    public  END_DATE : string
    public  PRIORITY : number
}

export class ProjectDetails{
    public  PROJECT_ID : string
    public  PROJECT_NAME : string
    public  START_DATE : string
    public  END_DATE : string
    public  PRIORITY : number
    public  NoOfTasks : number
    public  NoOfTasksCompleted : number
    public  Manager : UserTable
}
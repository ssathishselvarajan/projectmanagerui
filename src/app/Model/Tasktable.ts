export class TaskTable{
    public  TASK_ID : string
    public  PARENT_ID : string
    public  PROJECT_ID : string
    public  TASK : string
    public  START_DATE : string
    public  END_DATE : string
    public  PRIORITY : number
    public  STATUS : number
   
}
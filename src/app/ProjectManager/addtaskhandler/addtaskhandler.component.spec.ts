import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AddtaskhandlerComponent } from './addtaskhandler.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { FilterpipePipe } from 'src/app/Pipe/filterpipe.pipe';
import { OrderpipePipe } from 'src/app/Pipe/orderpipe.pipe';
import {HttpClientModule} from '@angular/common/http';
import { TaskTable } from 'src/app/model/taskTable';
import { of } from 'rxjs';
import { ProjectmanagerAPIService } from 'src/app/service/projectmanager-api.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

describe('AddtaskhandlerComponent', () => {
  let component: AddtaskhandlerComponent;
  let fixture: ComponentFixture<AddtaskhandlerComponent>;

  let getAllTaskSpy : any;
  let testAllTasks: TaskTable[];

  beforeEach(async(() => {
    const taskServiceSpy = jasmine.createSpyObj('ProjectmanagerAPIService',['getTaskByID'] );
    getAllTaskSpy = taskServiceSpy.getTaskByID.and.returnValue(of(testAllTasks));
    TestBed.configureTestingModule({
      imports:[FormsModule, ReactiveFormsModule,RouterTestingModule, HttpClientModule,NgbModule.forRoot()],
      declarations: [ AddtaskhandlerComponent, FilterpipePipe, OrderpipePipe  ],
      providers:[AddtaskhandlerComponent,{provide:ProjectmanagerAPIService, useValue:taskServiceSpy}]
    })
    .compileComponents();
    component = TestBed.get(AddtaskhandlerComponent);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddtaskhandlerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Test_getTaskByID', () =>{
    component.ngOnInit();
 expect(getAllTaskSpy.calls.any()).toBe(false, 'getTaskByID called');
  });
});

import { Component, OnInit } from '@angular/core';
import{ ProjectmanagerAPIService } from 'src/app/Service/projectmanager-api.service'
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { FormGroup, FormControl } from '@angular/forms';
import {Validators} from '@angular/forms';
import { TaskTable } from 'src/app/model/taskTable';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserformmodelComponent } from 'src/app/modelform/userformmodel/userformmodel.component';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap/modal/modal-ref';
import { UserTable } from 'src/app/model/userTable';
import {ActivatedRoute} from '@angular/router';



@Component({
  selector: 'app-addtaskhandler',
  templateUrl: './addtaskhandler.component.html',
  styleUrls: ['./addtaskhandler.component.css']
})
export class AddtaskhandlerComponent implements OnInit {
  taskForm = new FormGroup(
    {
        TASK_ID : new FormControl(''),
        PARENT_ID : new FormControl(''),
        PROJECT_ID : new FormControl('', Validators.required),
        TASK : new FormControl('', Validators.required),
        START_DATE :new FormControl(''),
        END_DATE : new FormControl(''),
        PRIORITY : new FormControl(''),
        STATUS : new FormControl('')
    }
    )
    newTask : TaskTable;
    isParentTask : boolean;
    selectedManager :UserTable;
    selectedManagerName : string = "";
    selectedProjectName : string = "";
    selectedParentName : string = "";
      constructor(private taskService : ProjectmanagerAPIService, private userService : ProjectmanagerAPIService, private projectService :ProjectmanagerAPIService,  private modalService: NgbModal, private route : ActivatedRoute) { }
    
      ngOnInit() {
        this.loadTask();
      }
    
      loadTask(){
        if(this.route.snapshot.paramMap.get('TaskID')){
         this.taskService.getTaskByID(this.route.snapshot.paramMap.get('TaskID')).subscribe(data=> 
          {
            if(data){
              this.taskForm.setValue(data);
              
              if(data.PROJECT_ID){
                this.projectService.getProjectByID(data.PROJECT_ID).subscribe(proData => {
                    if(proData){
                      this.selectedProjectName = proData.PROJECT1;
                    }
                });
              }
    
              if(data.PARENT_ID){
                this.taskService.getTaskByID(data.PARENT_ID).subscribe(tData => {
                  if(tData){
                    this.selectedParentName = tData.TASK;
                  }
              });
              }
              
              document.getElementById("btnSubmit").innerText = "Update Task";
            }
            else{
              console.log("Edit Task ID not found");
            }
          }
        );
        }
        else{
          console.log("Task ID not found");
        }
      }
      onSubmit(){
      
        if(this.taskForm.valid){
          debugger;
          this.newTask = this.taskForm.value;
          this.newTask.STATUS =0;
          if(!this.newTask.TASK_ID){
              //add Task
              this.taskService.addTask(this.newTask).subscribe(data=> {
                debugger;
                if(data.PROJECT_ID != "" && data.PROJECT_ID != null &&
                  data.TASK_ID != "" && data.TASK_ID != null)
                  {
                  console.log('Task Added');
                  if(this.selectedManagerName != ""){
                    this.selectedManager.Project_ID = data.PROJECT_ID;
                    this.selectedManager.Task_ID = data.TASK_ID;
                    this.userService.updateUser(this.selectedManager).subscribe(userData =>
                    {console.log('Manager Updated')});
                  } 
                  this.resetTaskForm();
                }
                else{
                  alert("Task not added");
                }
              });
          }
          else{
              //update Task
              this.taskService.updateTask(this.newTask).subscribe(data=> {
                if(data.TASK_ID != "" && data.TASK_ID != null)
                  {
                  console.log('Task Updated'); 
                  if(this.selectedManagerName != ""){
                    this.selectedManager.Project_ID = this.newTask.PROJECT_ID;
                    this.selectedManager.Task_ID = this.newTask.TASK_ID;
                    this.userService.updateUser(this.selectedManager).subscribe(userData =>
                    {console.log('Manager Updated')});
                  }
                  
                  this.resetTaskForm();
                }
                else{
                  alert("Task not added");
                }
              });
          }
           
        }
        else{
          alert('Please enter the valid details.');
        }
      }
    
      resetTaskForm(){
        this.taskForm.reset();
        this.selectedManager = null;
        this.selectedManagerName = "";
        this.selectedProjectName = "";
        this.selectedParentName = "";
        document.getElementById("btnSubmit").innerText = "Add Task";
      }
    
      searchProject(){
        const modalRef = this.modalService.open(UserformmodelComponent);
        modalRef.componentInstance.searchType = "project";
        modalRef.componentInstance.searchResult.subscribe((data)=> {
          console.log(data);
          debugger;
          this.selectedProjectName = data.PROJECT1;
          this.taskForm.patchValue({PROJECT_ID : data.PROJECT_ID});
       });
      }
      searchParent(){
        const modalRef = this.modalService.open(UserformmodelComponent);
        modalRef.componentInstance.searchType = "taskparent";
        modalRef.componentInstance.searchResult.subscribe((data)=> {
          console.log(data);
          this.selectedParentName = data.Parent_Task;
          this.taskForm.patchValue({Parent_ID : data.PARENT_ID});
       });
      }
      searchUser(){
        const modalRef = this.modalService.open(UserformmodelComponent);
        modalRef.componentInstance.searchType = "user";
        modalRef.componentInstance.searchResult.subscribe((data)=> {
          console.log(data);
          this.selectedManager = data;
          this.selectedManagerName = this.selectedManager.FIRST_NAME;
       });
      }
      isParentTaskChecked(event:any){
          this.isParentTask = event.target.checked;
          
      }
}

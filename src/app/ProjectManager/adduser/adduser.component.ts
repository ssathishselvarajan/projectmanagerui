import { Component, OnInit } from '@angular/core';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { FormGroup, FormControl } from '@angular/forms';
import {Validators} from '@angular/forms';
import { UserTable } from 'src/app/model/userTable';
import { ProjectmanagerAPIService } from 'src/app/service/projectmanager-api.service';

@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.css']
})
export class AdduserComponent implements OnInit {

  userForm = new FormGroup(
    {
        USER_ID : new FormControl(),
        FIRST_NAME : new FormControl(),
        LAST_NAME : new FormControl(),
        EMPLOYEE_ID : new FormControl(),
        PROJECT_ID : new FormControl(),
        TASK_ID : new FormControl(),
    }
  )
  newUser : UserTable;
  allUsers : UserTable[];
  txtSearchUser : string;
  sortingName: string;
  isDesc: boolean;
    constructor(private userService : ProjectmanagerAPIService) { }
  
    ngOnInit() {
      this.getAllUsers();
    }
  
    getAllUsers(){
      this.userService.getUsers().subscribe(data =>{this.allUsers = data});
   }
  
    onSubmit(){
      if(this.userForm.valid){ 
        this.newUser = this.userForm.value;
        if(!this.newUser.USER_ID){
            this.userService.addUser(this.newUser).subscribe(data=> 
              {
               debugger;
               if(data != null && data.USER_ID != null){
                 debugger;
                  this.allUsers.push(data);
                  alert("User added successfully");
                  this.resetUserForm();
                  this.getAllUsers();
               
                }
               else{
                 alert("User not added");
               }
              });
        }
        else{
          this.userService.updateUser(this.newUser).subscribe(data=> 
            { 
              if(data != null && data.USER_ID != null){
                this.allUsers.splice(this.allUsers.findIndex(a=>a.USER_ID == this.newUser.USER_ID),1);
                this.allUsers.push(data);
                this.resetUserForm();
                alert("User updated successfully");
                this.getAllUsers();
              }
              else{
                alert("User not updated");
              }
             
            });
            
        }
      }
      else{
        alert('Please enter the valid details.');
      }
    }
  
    resetUserForm(){
      this.userForm.reset();
      document.getElementById("btnSubmit").innerText = "Add User"
    }
  
    editUser(userToUpdate : UserTable){
      // this.projectForm.controls["btnAddUser"]
      debugger;
      this.userForm.setValue(userToUpdate);
      document.getElementById("btnSubmit").innerText = "Update User"
    }
  
    deleteUser(userToDelete : UserTable){
      this.userService.deleteUser(userToDelete.USER_ID).subscribe(data =>
        {
          if(data){
            this.allUsers.splice(this.allUsers.findIndex(a=>a.USER_ID == userToDelete.USER_ID),1);
            alert("User deleted successfully");
            this.getAllUsers();
          }
          else{
            alert("User is not deleted");
          }
        });
    }
  
    sort(name: string): void {
      if (name && this.sortingName !== name) {
        this.isDesc = false;
      } else {
        this.isDesc = !this.isDesc;
      }
      this.sortingName = name;
    }
  

}

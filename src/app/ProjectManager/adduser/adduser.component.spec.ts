import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { FilterpipePipe } from 'src/app/Pipe/filterpipe.pipe';
import { OrderpipePipe } from 'src/app/Pipe/orderpipe.pipe';
import { HttpClientModule } from '@angular/common/http';
import { UserTable } from 'src/app/model/userTable';
import { ProjectmanagerAPIService } from 'src/app/service/projectmanager-api.service';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';
import { AdduserComponent } from './adduser.component';

describe('AdduserComponent', () => {
  let component: AdduserComponent;
  let fixture: ComponentFixture<AdduserComponent>;
  let getAllUsersSpy : any;
  let testAllUsers: UserTable[];

  beforeEach(async(() => {
    const userServiceSpy = jasmine.createSpyObj('ProjectmanagerAPIService',['getUsers'] );
    getAllUsersSpy = userServiceSpy.getUsers.and.returnValue(of(testAllUsers));
    TestBed.configureTestingModule({
      imports:[FormsModule, ReactiveFormsModule,RouterTestingModule, HttpClientModule],
      declarations: [ AdduserComponent,FilterpipePipe, OrderpipePipe  ],
      providers:[AdduserComponent, {provide:ProjectmanagerAPIService, useValue:userServiceSpy}]
    })
    .compileComponents();
    component = TestBed.get(AdduserComponent);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdduserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Test_getAllUsers', () =>{
    component.ngOnInit();
 expect(getAllUsersSpy.calls.any()).toBe(true, 'getAllUsers called');
  });
});

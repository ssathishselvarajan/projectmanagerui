import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddprojectmanagerComponent } from './addprojectmanager.component';

import{} from 'jasmine';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { FilterpipePipe } from 'src/app/Pipe/filterpipe.pipe';
import { OrderpipePipe } from 'src/app/Pipe/orderpipe.pipe';
import {HttpClientModule} from '@angular/common/http';
import { ProjectTable } from 'src/app/model/projectTable';
import { of } from 'rxjs';
import { ProjectmanagerAPIService } from 'src/app/service/projectmanager-api.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';



describe('AddprojectmanagerComponent', () => {
  let component: AddprojectmanagerComponent;
  let fixture: ComponentFixture<AddprojectmanagerComponent>;
  let getAllProjectsSpy : any;
  let testAllProjects: ProjectTable[];


  beforeEach(async(() => {
    const projectServiceSpy = jasmine.createSpyObj('ProjectmanagerAPIService',['getProjectDetails'] );
    getAllProjectsSpy = projectServiceSpy.getProjectDetails.and.returnValue(of(testAllProjects));
    TestBed.configureTestingModule({
      imports:[FormsModule, ReactiveFormsModule,RouterTestingModule, HttpClientModule,NgbModule.forRoot()],
      declarations: [ AddprojectmanagerComponent, FilterpipePipe, OrderpipePipe  ],
      providers:[AddprojectmanagerComponent,{provide:ProjectmanagerAPIService, useValue:projectServiceSpy}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddprojectmanagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Test_getProjectDetails', () =>{
    component.ngOnInit();
 expect(getAllProjectsSpy.calls.any()).toBe(false, 'getProjectDetails called');
  });
});

import { Component, OnInit } from '@angular/core';
import{ FilterpipePipe} from  'src/app/Pipe/filterpipe.pipe'
import {OrderpipePipe} from 'src/app/Pipe/orderpipe.pipe'
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { FormGroup,FormControl, Validators } from '@angular/forms';
import { ProjectmanagerAPIService } from 'src/app/Service/projectmanager-api.service'; 
import { UserformmodelComponent } from 'src/app/modelform/userformmodel/userformmodel.component';
import {ProjectTable,ProjectDetails} from 'src/app/Model/Projecttable'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap/modal/modal-ref';
import { Console } from '@angular/core/src/console';
//import{ TaskTable } from 'src/app/model/Tasktable'
import{ UserTable } from 'src/app/model/Usertable'
import {formatDate} from '@angular/common';


@Component({
  selector: 'app-addprojectmanager',
  templateUrl: './addprojectmanager.component.html',
  styleUrls: ['./addprojectmanager.component.css']
})


export class AddprojectmanagerComponent implements OnInit {

    projectForm = new FormGroup({
    PROJECT_ID : new FormControl(),
    PROJECT_NAME : new FormControl('',Validators.required),
    START_DATE : new FormControl(),
    END_DATE : new FormControl(),
    PRIORITY : new FormControl(),
    NoOfTasks : new FormControl(),
    NoOfTasksCompleted : new FormControl(),
    Manager : new FormControl()
  })
  
  txtSearchProject: string;
  sortingName: string;
  isDesc: boolean;
  
  IsStartEndDateEnabled :boolean;
  newProject :ProjectDetails;
  allProjects : ProjectTable[] = [];
  allProjectDetails : ProjectDetails[] = [];
  selectedManager : UserTable;
  selectedManagerName : string = "";
    constructor(private projectService : ProjectmanagerAPIService,private modalService: NgbModal) { }
  
    ngOnInit() {
      this.getAllProjectDetails();
    }
    isDatesEnabled(event:any){
  this.IsStartEndDateEnabled = event.target.checked;
    }
  
  
    getAllProjectDetails(){
      this.projectService.getProjectDetails().subscribe(data =>{this.allProjectDetails = data});
      debugger;
   }
    getAllProjects(){
      this.projectService.getProjects().subscribe(data =>{this.allProjects = data});
   }
  
    onSubmit(){
     debugger;
      if(this.projectForm.valid){
        this.newProject = this.projectForm.value;
        //this.newProject.Manager.FIRST_NAME =this.selectedManagerName
        
        if (this.IsStartEndDateEnabled==false || this.IsStartEndDateEnabled==null)
        {
          this.newProject.START_DATE = formatDate(new Date(), 'MM/dd/yyyy', 'en');
          this.newProject.END_DATE =formatDate(new Date().getDate()+1  , 'MM/dd/yyyy', 'en');
        }
        if(!this.newProject.PROJECT_ID){
            //add new project
            this.projectService.addProject(this.newProject).subscribe(data=> 
              {
                
                if(data.PROJECT_ID != "" && data.PROJECT_ID != null){
               alert('Project Added'); 
                this.allProjectDetails.push(data);
                if(this.selectedManagerName != ""){
                  this.selectedManager.Project_ID = data.PROJECT_ID;
                  this.projectService.updateUser(this.selectedManager).subscribe(userData =>
                  {alert('Manager Updated')
                  this.getAllProjectDetails();
                });
                }
                this.resetProjectForm();
              }
              else{
                alert("Project not added");
              }
              });

        }
        else{
            //update existing project
            debugger;
            this.projectService.updateProject(this.newProject).subscribe(data=> 
              {
                
                if(data.PROJECT_ID != "" && data.PROJECT_ID != null){
                console.log('Project Updated'); 
                this.allProjectDetails.splice(this.allProjects.findIndex(a=>a.PROJECT1 == this.newProject.PROJECT_ID),1);
                this.allProjectDetails.push(data);
               if(this.selectedManagerName != ""){
                  this.selectedManager.Project_ID = data.PROJECT_ID;
                 
                  this.projectService.updateUser(this.selectedManager).subscribe(userData =>
                  {console.log('Manager Updated')});
                }
                this.resetProjectForm();
                }
                else{
                  alert("Project not updated");
                }
              });
        }
         
      }
      else{
        alert('Please enter the valid details.');
      }
    }
    searchUser(){
      
      const modalRef = this.modalService.open(UserformmodelComponent);
      modalRef.componentInstance.searchType = "user";
      modalRef.componentInstance.searchResult.subscribe((data)=> {
        console.log(data);
        this.selectedManager = data;
        debugger;
        this.selectedManagerName = this.selectedManager.FIRST_NAME;
        this.projectForm.patchValue({Manager : this.selectedManager.FIRST_NAME});
     });
    }
  
    resetProjectForm(){
      this.projectForm.reset();
      this.selectedManager = null;
      this.selectedManagerName = "";
      document.getElementById("btnSubmit").innerHTML = "Add Project"
    }
  
    updateProject(projectToUpdate : ProjectDetails){
      // const projToEdit = new ProjectTable;
      // projToEdit.PROJECT_ID = projectToUpdate.PROJECT_ID;
      // projToEdit.PROJECT1 = projectToUpdate.PROJECT_NAME;
      // projToEdit.START_DATE = projectToUpdate.START_DATE;
      // projToEdit.END_DATE = projectToUpdate.END_DATE;
      // projToEdit.PRIORITY = projectToUpdate.PRIORITY;
      debugger;
      if(projectToUpdate.Manager){
        this.selectedManager = projectToUpdate.Manager;
        this.selectedManagerName = projectToUpdate.Manager.FIRST_NAME;
      }
      this.projectForm.setValue(projectToUpdate);
      document.getElementById("btnSubmit").innerText = "Update Project"
    } 
  
    suspendProject(projectToSuspend : ProjectDetails){
      this.projectService.deleteProject(projectToSuspend.PROJECT_ID).subscribe(data =>
        {
          if(data){
            this.allProjects.splice(this.allProjects.findIndex(a=>a.PROJECT_ID == projectToSuspend.PROJECT_ID),1);
            this.getAllProjectDetails();
          }
          else{
            alert("Project is not deleted");
          }
        });
    }
  
    sort(name: string): void {
      if (name && this.sortingName !== name) {
        this.isDesc = false;
      } else {
        this.isDesc = !this.isDesc;
      }
      this.sortingName = name;
    }

}

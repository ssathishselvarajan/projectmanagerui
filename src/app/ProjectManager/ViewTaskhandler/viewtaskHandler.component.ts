import { Component, OnInit } from '@angular/core';
import{ ProjectTable } from 'src/app/Model/Projecttable'
import { Data } from '@angular/router/src/config';
import {Observable} from 'rxjs';
import{ ProjectmanagerAPIService } from 'src/app/Service/projectmanager-api.service'
import { TaskTable } from 'src/app/model/taskTable';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserformmodelComponent } from 'src/app/modelform/userformmodel/userformmodel.component';

@Component({
  selector: 'app-home',
  templateUrl: './viewtaskHandler.component.html',
  styleUrls: ['./viewtaskHandler.component.css']
})
export class ViewtaskHandler implements OnInit {
  txtProject : string;
  txtProjectID : string;
  txtParentTask : number;
  txtPrioriyFrom : number;
  txtPrioriyTo : number;
  dtStartDate : Date;
  dtEndDate : Date;

  allTask : TaskTable[]=[];
  sortingName: string;
  isDesc: boolean;
  constructor(private taskService : ProjectmanagerAPIService, private modalService: NgbModal) { }

  ngOnInit() {
    //this.getAllTasks();
  }

//   getAllTasks(){
//     this.taskService. getTasks().subscribe(data =>{debugger;this.allTask = data});
//  }

  getTasksByProjectID(projectID : string){
     this.taskService.getTasksByProjectID(projectID).subscribe(data =>{
      debugger; 
      this.allTask = data;
   
    }
    
      );

     
     
  }



  endTask(id:any){
    let taskToEnd : TaskTable;
    if(this.allTask && this.allTask.length > 0)
    {
      taskToEnd = this.allTask.find(a=>a.TASK_ID == id);
      if(taskToEnd){
        taskToEnd.STATUS = 1;
        this.taskService.updateTask(taskToEnd).subscribe(data=> {
          if(data){
            alert("Task updated");
          }
          else{
            alert("Task is not updated");
          }
        });
      }
    }
  }

  deleteTask(id:string){
    this.taskService.deleteTask(id).subscribe(data=> {
      
     if(data){
       this.allTask.splice(this.allTask.findIndex(a=>a.TASK_ID == id),1);
     }
     else{
       alert("Task is not deleted");
     }
     });
   }

   searchProject(){
    const modalRef = this.modalService.open(UserformmodelComponent);
    modalRef.componentInstance.searchType = "project";
    modalRef.componentInstance.searchResult.subscribe((data)=> {
      console.log(data);
      this.txtProject = data.PROJECT1;
      this.txtProjectID = data.PROJECT_ID;
      this.getTasksByProjectID(data.PROJECT_ID);
   });
  }

  sort(name: string): void {
    if (name && this.sortingName !== name) {
      this.isDesc = false;
    } else {
      this.isDesc = !this.isDesc;
    }
    this.sortingName = name;
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import{ TaskTable } from 'src/app/Model/Tasktable'
import { ViewtaskHandler } from './viewtaskHandler.component';
import { ProjectmanagerAPIService } from 'src/app/service/projectmanager-api.service';
import { of } from 'rxjs';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import{RouterTestingModule} from '@angular/router/testing';
import { FilterpipePipe } from 'src/app/Pipe/filterpipe.pipe';
import { OrderpipePipe } from 'src/app/Pipe/orderpipe.pipe'
import{ HttpClientModule } from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';


describe('viewtaskHandler', () => {
  let component: ViewtaskHandler;
  let fixture: ComponentFixture<ViewtaskHandler>;

  let getAllTasksSpy : any;
  let testAllTasks: TaskTable[];

  beforeEach(async(() => {
    const taskServiceSpy = jasmine.createSpyObj('ServiceTaskService',['getTasks'] );
    getAllTasksSpy = taskServiceSpy.getTasks.and.returnValue(of(testAllTasks));
    TestBed.configureTestingModule({
      imports:[FormsModule, ReactiveFormsModule,RouterTestingModule,HttpClientModule,NgbModule.forRoot()],
      declarations: [ ViewtaskHandler,FilterpipePipe, OrderpipePipe ],
      providers:[ViewtaskHandler, {provide:ProjectmanagerAPIService, useValue:taskServiceSpy}]
    })
    .compileComponents();
    component = TestBed.get(ViewtaskHandler);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewtaskHandler);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Test_GetAllTasks', () =>{
    component.ngOnInit();
 expect(getAllTasksSpy.calls.any()).toBe(false, 'GetAllTasks called');
  });
});

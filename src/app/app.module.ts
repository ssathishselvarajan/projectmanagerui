import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {RouterModule, Routes} from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ViewtaskHandler } from './ProjectManager/ViewTaskhandler/viewtaskHandler.component';
import { AddtaskhandlerComponent } from './ProjectManager/addtaskhandler/addtaskhandler.component';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { MenuComponent } from './ProjectManager/menu/menu.component';
import{ HttpClientModule } from '@angular/common/http';
import { ProjectmanagerAPIService } from 'src/app/Service/projectmanager-api.service';
import { AddprojectmanagerComponent } from './ProjectManager/addprojectmanager/addprojectmanager.component';
import { AdduserComponent } from './ProjectManager/adduser/adduser.component';
import { FilterpipePipe } from './Pipe/filterpipe.pipe';
import { OrderpipePipe } from './Pipe/orderpipe.pipe';
import { UserformmodelComponent } from './modelform/userformmodel/userformmodel.component';


@NgModule({
  declarations: [
    AppComponent,
    ViewtaskHandler,
    AddtaskhandlerComponent,
    MenuComponent,
    AddprojectmanagerComponent,
    AdduserComponent,
    FilterpipePipe,
    OrderpipePipe,
    UserformmodelComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    FormsModule,ReactiveFormsModule,HttpClientModule,RouterModule,AppRoutingModule
  ],
  exports: [
    BrowserModule,
    FormsModule,ReactiveFormsModule
  ],
  providers: [ProjectmanagerAPIService],
  bootstrap: [AppComponent],
  entryComponents : [UserformmodelComponent]
  
})
export class AppModule { }
